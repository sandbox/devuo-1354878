<?php


/**
 * Plugin definition.
 */
$plugin = array(
  'title' => t('Foreign Tabs'),
  'category' => t('Page elements'),
  'description' => t('Prints tabs from other content.'),
  'defaults' => '',
);

/**
 * Implements hook_file_content_type_render().
 */
function foreign_tabs_foreign_tabs_content_type_render($subtype, $conf, $args, $context) {
  
  $path = empty($conf['path']) ? '' : $conf['path'];
  
  $panel = panels_get_current_page_display();
  
  if ($path) {
    $path = ctools_context_keyword_substitute($path, array(), $panel->context);
  }
  
  $tabs = foreign_tabs_local_tasks($path);
  $tabs = array('primary' => $tabs['tabs']['output']);

  $block = new stdClass();
  $block->module = 'foreign_tabs';
  $block->title = t('Foreign Tabs');

  $block->content = theme('menu_local_tasks', $tabs);
  return $block;
}

/**
 * Implements hook_file_content_type_edit_form().
 */
function foreign_tabs_foreign_tabs_content_type_edit_form($form, &$form_state) {
  $conf = $form_state['conf'];

  $form['path'] = array(
    '#title' => t('Path'),
    '#description' => t('This path will be used to fetch the path from. Replacement tokens can be used.'),
    '#type' => 'textfield',
    '#default_value' => $conf['path'],
    '#description' => t('If left empty, the local tab will be used.'),
  );
  return $form;
}

/**
 * Implements hook_file_content_type_form_submit().
 */
function foreign_tabs_foreign_tabs_content_type_edit_form_submit($form, &$form_state) {
  $form_state['conf']['path'] = $form_state['values']['path'];
}
